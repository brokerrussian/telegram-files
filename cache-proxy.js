const $request = require('request');
const $qs = require('qs');

module.exports = function (req, res) {
  const path = req.params[0];
  const cacheProxy = [/^embed/, /^file/];
  const isProxy = cacheProxy.reduce((f, regExp) => {
    if (path.match(regExp)) {
      f = true;
    }

    return f;
  }, false);

  const source = `http://telegra.ph/${path}?${$qs.stringify(req.query)}`;

  if (!isProxy) {
    return res.redirect(source);
  }

  res.writeHead(200, {
    'Cache-Control': 'max-age=2678400000'
  });

  $request
      .get(source)
      .pipe(res);
  
};